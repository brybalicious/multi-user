# ##### BEGIN GPL LICENSE BLOCK #####
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

import logging

import bpy

from . import utils
from .presence import (renderer,
                       UserFrustumWidget,
                       UserNameWidget,
                       UserSelectionWidget,
                       refresh_3d_view,
                       generate_user_camera,
                       get_view_matrix,
                       refresh_sidebar_view)
from . import  operators
from replication.constants import (FETCHED,
                                   UP,
                                   RP_COMMON,
                                   STATE_INITIAL,
                                   STATE_QUITTING,
                                   STATE_ACTIVE,
                                   STATE_SYNCING,
                                   STATE_LOBBY,
                                   STATE_SRV_SYNC)

from replication.interface import session
from replication.exception import NonAuthorizedOperationError


def is_annotating(context: bpy.types.Context):
    """ Check if the annotate mode is enabled
    """
    return bpy.context.workspace.tools.from_space_view3d_mode('OBJECT', create=False).idname == 'builtin.annotate'

class Delayable():
    """Delayable task interface
    """

    def register(self):
        raise NotImplementedError

    def execute(self):
        raise NotImplementedError

    def unregister(self):
        raise NotImplementedError


class Timer(Delayable):
    """Timer binder interface for blender

    Run a bpy.app.Timer in the background looping at the given rate
    """

    def __init__(self, duration=1):
        super().__init__()
        self._timeout = duration
        self.is_running = False

    def register(self):
        """Register the timer into the blender timer system
        """

        if not self.is_running:
            bpy.app.timers.register(self.main)
            self.is_running = True
            logging.debug(f"Register {self.__class__.__name__}")
        else:
            logging.debug(
                f"Timer {self.__class__.__name__} already registered")

    def main(self):
        try:
            self.execute()
        except Exception as e:
            logging.error(e)
            self.unregister()
            session.disconnect()
        else:    
            if self.is_running:
                return self._timeout

    def execute(self):
        """Main timer loop
        """
        raise NotImplementedError

    def unregister(self):
        """Unnegister the timer of the blender timer system
        """
        if bpy.app.timers.is_registered(self.main):
            bpy.app.timers.unregister(self.main)

        self.is_running = False


class ApplyTimer(Timer):
    def __init__(self, timout=1, target_type=None):
        self._type = target_type
        super().__init__(timout)

    def execute(self):
        if session and session.state['STATE'] == STATE_ACTIVE:
            if self._type:
                nodes = session.list(filter=self._type)
            else:
                nodes = session.list()

            for node in nodes:
                node_ref = session.get(uuid=node)

                if node_ref.state == FETCHED:
                    try:
                        session.apply(node)
                    except Exception as e:
                        logging.error(f"Fail to apply {node_ref.uuid}: {e}")
                    else:
                        if self._type.bl_reload_parent:
                            parents = []

                            for n in session.list():
                                deps = session.get(uuid=n).dependencies
                                if deps and node in deps:
                                    session.apply(n, force=True)

class DynamicRightSelectTimer(Timer):
    def __init__(self, timout=.1):
        super().__init__(timout)
        self._last_selection = []
        self._user = None
        self._annotating = False

    def execute(self):
        settings = utils.get_preferences()

        if session and session.state['STATE'] == STATE_ACTIVE:
            # Find user
            if self._user is None:
                self._user = session.online_users.get(settings.username)

            if self._user:
                ctx = bpy.context
                annotation_gp = ctx.scene.grease_pencil

                # if an annotation exist and is tracked
                if annotation_gp and annotation_gp.uuid:
                    registered_gp = session.get(uuid=annotation_gp.uuid)
                    if is_annotating(bpy.context):
                        # try to get the right on it
                        if registered_gp.owner == RP_COMMON:
                            self._annotating = True
                            logging.debug(
                                "Getting the right on the annotation GP")
                            session.change_owner(
                                registered_gp.uuid,
                                settings.username,
                                ignore_warnings=True,
                                affect_dependencies=False)
                    elif self._annotating:
                        session.change_owner(
                            registered_gp.uuid,
                            RP_COMMON,
                            ignore_warnings=True,
                            affect_dependencies=False)

                current_selection = utils.get_selected_objects(
                    bpy.context.scene,
                    bpy.data.window_managers['WinMan'].windows[0].view_layer
                )
                if current_selection != self._last_selection:
                    obj_common = [
                        o for o in self._last_selection if o not in current_selection]
                    obj_ours = [
                        o for o in current_selection if o not in self._last_selection]

                    # change old selection right to common
                    for obj in obj_common:
                        node = session.get(uuid=obj)

                        if node and (node.owner == settings.username or node.owner == RP_COMMON):
                            recursive = True
                            if node.data and 'instance_type' in node.data.keys():
                                recursive = node.data['instance_type'] != 'COLLECTION'
                            try:
                                session.change_owner(
                                    node.uuid,
                                    RP_COMMON,
                                    ignore_warnings=True,
                                    affect_dependencies=recursive)
                            except NonAuthorizedOperationError:
                                logging.warning(
                                    f"Not authorized to change {node} owner")

                    # change new selection to our
                    for obj in obj_ours:
                        node = session.get(uuid=obj)

                        if node and node.owner == RP_COMMON:
                            recursive = True
                            if node.data and 'instance_type' in node.data.keys():
                                recursive = node.data['instance_type'] != 'COLLECTION'

                            try:
                                session.change_owner(
                                    node.uuid,
                                    settings.username,
                                    ignore_warnings=True,
                                    affect_dependencies=recursive)
                            except NonAuthorizedOperationError:
                                logging.warning(
                                    f"Not authorized to change {node} owner")
                        else:
                            return

                    self._last_selection = current_selection

                    user_metadata = {
                        'selected_objects': current_selection
                    }

                    session.update_user_metadata(user_metadata)
                    logging.debug("Update selection")

                    # Fix deselection until right managment refactoring (with Roles concepts)
                    if len(current_selection) == 0 :
                        owned_keys = session.list(
                            filter_owner=settings.username)
                        for key in owned_keys:
                            node = session.get(uuid=key)
                            try:
                                session.change_owner(
                                    key,
                                    RP_COMMON,
                                    ignore_warnings=True,
                                    affect_dependencies=recursive)
                            except NonAuthorizedOperationError:
                                logging.warning(
                                    f"Not authorized to change {key} owner")

            for obj in bpy.data.objects:
                object_uuid = getattr(obj, 'uuid', None)
                if object_uuid:
                    is_selectable = not session.is_readonly(object_uuid)
                    if obj.hide_select != is_selectable:
                        obj.hide_select = is_selectable


class ClientUpdate(Timer):
    def __init__(self, timout=.1):
        super().__init__(timout)
        self.handle_quit = False
        self.users_metadata = {}

    def execute(self):
        settings = utils.get_preferences()

        if session and renderer:
            if session.state['STATE'] in [STATE_ACTIVE, STATE_LOBBY]:
                local_user = session.online_users.get(
                    settings.username)

                if not local_user:
                    return
                else:
                    for username, user_data in session.online_users.items():
                        if username != settings.username:
                            cached_user_data = self.users_metadata.get(
                                username)
                            new_user_data = session.online_users[username]['metadata']

                            if cached_user_data is None:
                                self.users_metadata[username] = user_data['metadata']
                            elif 'view_matrix' in cached_user_data and 'view_matrix' in new_user_data and cached_user_data['view_matrix'] != new_user_data['view_matrix']:
                                refresh_3d_view()
                                self.users_metadata[username] = user_data['metadata']
                                break
                            else:
                                self.users_metadata[username] = user_data['metadata']

                local_user_metadata = local_user.get('metadata')
                scene_current = bpy.context.scene.name
                local_user = session.online_users.get(settings.username)
                current_view_corners = generate_user_camera()

                # Init client metadata
                if not local_user_metadata or 'color' not in local_user_metadata.keys():
                    metadata = {
                        'view_corners': get_view_matrix(),
                        'view_matrix': get_view_matrix(),
                        'color': (settings.client_color.r,
                                  settings.client_color.g,
                                  settings.client_color.b,
                                  1),
                        'frame_current': bpy.context.scene.frame_current,
                        'scene_current': scene_current
                    }
                    session.update_user_metadata(metadata)

                # Update client representation
                # Update client current scene
                elif scene_current != local_user_metadata['scene_current']:
                    local_user_metadata['scene_current'] = scene_current
                    session.update_user_metadata(local_user_metadata)
                elif 'view_corners' in local_user_metadata and current_view_corners != local_user_metadata['view_corners']:
                    local_user_metadata['view_corners'] = current_view_corners
                    local_user_metadata['view_matrix'] = get_view_matrix(
                    )
                    session.update_user_metadata(local_user_metadata)


class SessionStatusUpdate(Timer):
    def __init__(self, timout=1):
        super().__init__(timout)

    def execute(self):
        refresh_sidebar_view()


class SessionUserSync(Timer):
    def __init__(self, timout=1):
        super().__init__(timout)
        self.settings = utils.get_preferences()

    def execute(self):
        if session and renderer:
            # sync online users
            session_users = session.online_users
            ui_users = bpy.context.window_manager.online_users

            for index, user in enumerate(ui_users):
                if user.username not in session_users.keys() and \
                        user.username != self.settings.username:
                    renderer.remove_widget(f"{user.username}_cam")
                    renderer.remove_widget(f"{user.username}_select")
                    renderer.remove_widget(f"{user.username}_name")
                    ui_users.remove(index)
                    break

            for user in session_users:
                if user not in ui_users:
                    new_key = ui_users.add()
                    new_key.name = user
                    new_key.username = user
                    if user != self.settings.username:
                        renderer.add_widget(
                            f"{user}_cam", UserFrustumWidget(user))
                        renderer.add_widget(
                            f"{user}_select", UserSelectionWidget(user))
                        renderer.add_widget(
                            f"{user}_name", UserNameWidget(user))


class MainThreadExecutor(Timer):
    def __init__(self, timout=1, execution_queue=None):
        super().__init__(timout)
        self.execution_queue = execution_queue

    def execute(self):
        while not self.execution_queue.empty():
            function, kwargs = self.execution_queue.get()
            logging.debug(f"Executing {function.__name__}")
            function(**kwargs)