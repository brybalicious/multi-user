# Changelog

All notable changes to this project will be documented in this file.

## [0.0.2] - 2020-02-28

### Added

- Blender animation features support (alpha).
  - Action.
  - Armature (Unstable).
  - Shape key.
  - Drivers.
  - Constraints.
- Snap to user timeline tool.
- Light probes support (only since 2.83).
- Metaballs support.
- Improved modifiers support.
- Online documentation.
- Improved Undo handling.
- Improved overall session handling:
  - Time To Leave : ensure clients/server disconnect automatically on connection lost.
  - Ping: show clients latency.
  - Non-blocking connection.
  - Connection state tracking.
- Service communication layer to manage background daemons.

### Changed

- UI revamp:
  - Show users frame.
  - Expose IPC(inter process communication) port.
  - New user list.
  - Progress bar to track connection status.
- Right management takes view-layer in account for object selection.
- Use a basic BFS approach for replication graph pre-load.
- Serialization is now based on marshal (2x performance improvements).
- Let pip chose python dependencies install path.

## [0.0.3] - 2020-07-29

### Added

- Auto updater support
- Big Performances improvements on Meshes, Gpencils, Actions
- Multi-scene workflow support
- Render setting synchronization
- Kick command
- Dedicated server with a basic command set
- Administrator session status
- Tests
- Blender 2.83-2.90 support

### Changed

- Config is now stored in blender user preference
- Documentation update
- Connection protocol
- UI revamp:
  - user localization
  - repository init

### Removed

- Unused strict right management strategy
- Legacy config management system

## [0.1.0] - 2020-10-05

### Added

- Dependency graph driven updates [experimental]
- Edit Mode updates
- Late join mechanism 
- Sync Axis lock replication
- Sync collection offset
- Sync camera  orthographic scale
- Sync custom fonts
- Sync sound files
- Logging configuration (file output and level)
- Object visibility type replication
- Optionnal sync for active camera
- Curve->Mesh conversion
- Mesh->gpencil conversion

### Changed

- Auto updater now handle installation from branches
- Use uuid for collection loading
- Moved session instance to replication package

### Fixed

- Prevent unsupported data types to crash the session
- Modifier vertex group assignation
- World sync
- Snapshot UUID error
- The world is not synchronized

## [0.1.1] - 2020-10-16

### Added

- Session status widget
- Affect dependencies during change owner
- Dedicated server managment scripts(@brybalicious)

### Changed

- Refactored presence.py
- Reset button UI icon 
- Documentation `How to contribute` improvements (@brybalicious)
- Documentation `Hosting guide` improvements (@brybalicious)
- Show flags are now available from the viewport overlay
 
### Fixed

- Render sync race condition (causing scene errors)
- Binary differentials
- Hybrid session crashes between Linux/Windows
- Materials node default output value
- Right selection
- Client node rights changed to COMMON after disconnecting from the server 
- Collection instances selection draw
- Packed image save error
- Material replication
- UI spelling errors (@brybalicious)


## [0.2.0] - 2020-12-17

### Added

- Documentation `Troubleshouting` section (@brybalicious)
- Documentation `Update` section (@brybalicious)
- Documentation `Cloud Hosting Walkthrough` (@brybalicious)
- Support DNS name
- Sync annotations
- Sync volume objects
- Sync material node_goups 
- Sync VSE
- Sync grease pencil modifiers
- Sync textures (modifier only)
- Session status widget
- Disconnection popup 
- Popup with disconnection reason 


### Changed

- Improved GPencil performances

### Fixed

- Texture paint update
- Various documentation fixes section (@brybalicious)
- Empty and Light object selection highlights
- Material renaming
- Default material nodes input parameters
- blender 2.91 python api compatibility